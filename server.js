var express = require('express');
var bodyParser = require('body-parser');
var https = require('https');
var app     = express();
var querystring = require('querystring');

//Note that in version 4 of express, express.bodyParser() was
//deprecated in favor of a separate 'body-parser' module.
app.use(bodyParser.urlencoded({ extended: true }));

//app.use(express.bodyParser());

app.post('/getHotels', function(req, res) {
  console.log(req.body.city)

  //getHotelRates(); In an ideal world,  we will get rates via this web service call.
  result = bestRateHotel(snaptravelPrices, hotelscomPrices);
  res.render('showHotels', {result: result});
});

app.post('/', function(req, res) {
  res.send('<html><body>Hello</body>')
  bestRateHotel();
});

app.get('/', function(req, res) {
  bestRateHotel();
  res.sendfile('index.html')
});

app.listen(8080, function() {
  console.log('Server running at http://127.0.0.1:8080/');
});

function getHotelRates() {
  var requestBody = {
    "city" : "Boston",
    "checkin" : "2018-04-01",
    "checkout" : "2018-04-04",
    "provider" : "snaptravel"
  }
  var post_data = querystring.stringify({
      'city' : 'ADVANCED_OPTIMIZATIONS',
      'checkin': 'json',
      'checkout': 'compiled_code',
      'provider' : 'snaptravel'
  });

  var post_options = {
      host: 'experimentation.getsnaptravel.com',
      path: '/interview/hotels',
      method: 'POST',
      headers : {
        'Content-Type' : 'application/json'
      },
    data: requestBody
  };

  // Set up the request
  var post_req = https.request(post_options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
          console.log('Response: ' + chunk);
      });
  });
}

//
function bestRateHotel(snaptravel, hotels) {
  var snaptravel = {
    "hotels": [
        {
            "id": 22,
            "hotel_name": "The Grand Budapest Hotel",
            "num_reviews": 1372,
            "address": "231 Boylston St, Very Large City",
            "num_stars": 5,
            "amenities": [
                "Parking",
                "Pool"
            ],
            "image_url": "https://images.trvl-media.com/hotels/1000000/910000/904900/904821/904821_178_b.jpg",
            "price": 244.99
        },
        {
            "id": 81,
            "hotel_name": "Famous Lucky Dragon Hotel",
            "num_reviews": 1398,
            "address": "132 Sunvale Ave, Very Large City",
            "num_stars": 1,
            "amenities": [
                "Parking"
            ],
            "image_url": "https://images.trvl-media.com/hotels/1000000/10000/7700/7611/7611_103_b.jpg",
            "price": 210.99
        }
      ]};

  var hotels = {
    "hotels": [
        {
            "id": 87,
            "hotel_name": "The Seven Seasons Hotel",
            "num_reviews": 338,
            "address": "12 Main Street, Very Large City",
            "num_stars": 5,
            "amenities": [
                "Wi-Fi",
                "Pool",
                "Breakfast"
            ],
            "image_url": "https://images.trvl-media.com/hotels/1000000/50000/41300/41245/8640cd6f_b.jpg",
            "price": 101
        },
        {
           "id": 81,
           "hotel_name": "Famous Lucky Dragon Hotel",
           "num_reviews": 1398,
           "address": "132 Sunvale Ave, Very Large City",
           "num_stars": 1,
           "amenities": [
               "Parking"
           ],
           "image_url": "https://images.trvl-media.com/hotels/1000000/10000/7700/7611/7611_103_b.jpg",
           "price": 167.99
       }]};

  result = []

  for(hotel in snaptravel.hotels) {
    console.log(snaptravel.hotels[hotel])
    var id = snaptravel.hotels[hotel].id;
    console.log("id is:"+id);
    for(hotel1 in hotels.hotels) {
      var hotelsObj = hotels.hotels[hotel1];
      if (hotelsObj.id === id) {
        console.log("Matched id is :"+hotelsObj.id);
        snaptravelPrice = snaptravel.hotels[hotel].price
        hotelsObj["snaptravel_price"] = snaptravelPrice
        result.push(hotelsObj)
      }
    }
  }

  console.log(result)
}
