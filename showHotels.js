table_data = $("#table_data")
for(hotel in result) {
  var hotel_id = document.createElement("td");
  var hotel_name = document.createElement("td");
  var number_of_reviews = document.createElement("td");
  var number_of_stars = document.createElement("td");
  var amenities = document.createElement("td");
  var image = document.createElement("td");
  var snaptravel_price = document.createElement("td");
  var hotelscom_price = document.createElement("td");

  hotel_id.innerHTML = hotel.id;
  hotel_name.innerHTML = hotel.hotel_name;
  number_of_reviews.innerHTML = hotel.num_reviews;
  number_of_stars.innerHTML = hotel.num_stars;
  amenities.innerHTML = hotel.amenities;
  image.innerHTML = hotel.image_url;
  snaptravelPrice.innerHTML = hotel.snaptravelPrice;
  hotelscom_price.innerHTML = hotel.price

  table_data.append(hotel_id, hotel_name, number_of_reviwes, number_of_stars, amenities, image, snaptravel_price, hotelscom_price);
}
